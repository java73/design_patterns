unit b_Chain_of_responsibility;

interface

uses SysUtils;

type

  TPurchase = class
  public
    Number: integer;
    Amount: Double;
    Purpose: string;
    constructor Create(num: integer; am: Double; pur: string);
  end;

  IApprover = interface
  ['{3ACA3967-FFCF-48A1-AC45-9A9B98A8DD96}']
    procedure SetSuccessor(successor: IApprover);
    procedure ProcessRequest(purchase: TPurchase);
  end;

  TApprover = class(TInterfacedObject, IApprover)
  protected
    FSuccessor: IApprover;
  public
    procedure SetSuccessor(successor: IApprover);
    procedure ProcessRequest(purchase: TPurchase); virtual; abstract;
  end;

  TDirector = class(TApprover)
    procedure ProcessRequest(purchase: TPurchase); override;
  end;

  TVicePresident = class(TApprover)
    procedure ProcessRequest(purchase: TPurchase); override;
  end;

  TPresident = class(TApprover)
    procedure ProcessRequest(purchase: TPurchase); override;
  end;

implementation

{ TApprover }

procedure TApprover.SetSuccessor(successor: IApprover);
begin
  FSuccessor := successor;
end;

{ TDirector }

procedure TDirector.ProcessRequest(purchase: TPurchase);
begin
  if purchase.Amount < 10000.0 then
    WriteLn(Format('Director approved request # %d', [purchase.Number]))
  else if FSuccessor <> nil then
    FSuccessor.ProcessRequest(purchase);
end;

{ TVicePresident }

procedure TVicePresident.ProcessRequest(purchase: TPurchase);
begin
  if purchase.Amount < 25000.0 then
    WriteLn(Format('VicePresident approved request # %d', [purchase.Number]))
  else if FSuccessor <> nil then
    FSuccessor.ProcessRequest(purchase);
end;

{ TPresident }

procedure TPresident.ProcessRequest(purchase: TPurchase);
begin
  if purchase.Amount < 100000.0 then
    WriteLn(Format('President approved request # %d', [purchase.Number]))
  else 
    WriteLn(Format('Request# %d requires an executive meeting!', [purchase.Number]))
end;

{ TPurchase }

constructor TPurchase.Create(num: integer; am: Double; pur: string);
begin
  Number := num;
  Amount := am;
  Purpose := pur;
end;

end.
unit b_Command;

interface

uses SysUtils, Classes;

type

  TCalculator = class
  private
    FCurr: Double;
  public
    constructor Create;
    procedure Operation(oper: char; operand: integer);
  end;

  ICommand = interface
  ['{19AEB63B-144F-46E7-A452-64713080D7A2}']
    procedure Execute();
    procedure UnExecute();
  end;

  TCalculatorCommand = class(TInterfacedObject, ICommand)
  private
    FOperator: char;
    FOperand: integer;
    FCalculator: TCalculator;
    function Undo(oper: char): char;
  public
    constructor Create(calc: TCalculator; oper: char; operand: integer);
    procedure Execute();
    procedure UnExecute();
  end;

  TUser = class
  private
    FCalculator: TCalculator;
    FCommands: TInterfaceList;
    FCurrent: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Redo(levels: integer);
    procedure Undo(levels: integer);
    procedure Compute(oper: char; operand: integer);
  end;

implementation

{ TCalculator }

constructor TCalculator.Create;
begin
  inherited;
  FCurr := 0;
end;

procedure TCalculator.Operation(oper: char; operand: integer);
begin
  case oper of
    '+': FCurr := FCurr + operand;
    '-': FCurr := FCurr - operand;
    '*': FCurr := FCurr * operand;
    '/': FCurr := FCurr / operand;
  end;
  WriteLn(Format('Current Value = %f (following %s, %d)', [FCurr, oper, operand]));
end;

{ TCalculatorCommand }

constructor TCalculatorCommand.Create(calc: TCalculator; oper: char;
  operand: integer);
begin
  FCalculator := calc;
  FOperator := oper;
  FOperand := operand;
end;

procedure TCalculatorCommand.Execute;
begin
  FCalculator.Operation(FOperator, FOperand);
end;

function TCalculatorCommand.Undo(oper: char): char;
begin
  case oper of
    '+': Result := '-';
    '-': Result := '+';
    '*': Result := '/';
    '/': Result := '*';
    else
      raise Exception.Create(Format('%s is not a valid operator', [oper]));
  end;
end;

procedure TCalculatorCommand.UnExecute;
begin
  FCalculator.Operation(Undo(FOperator), FOperand);
end;

{ TUser }

constructor TUser.Create;
begin
  FCalculator := TCalculator.Create;
  FCommands := TInterfaceList.Create;
  FCurrent := 0;
end;

destructor TUser.Destroy;
begin
  FCalculator.Free;
  FCommands.Free;
  inherited;
end;

procedure TUser.Redo(levels: integer);
var
  i: integer;
  command: ICommand;
begin
  WriteLn(#10 + Format('----- Redo %d levels ', [levels]));
  for i := 0 to levels - 1 do begin
    if FCurrent < FCommands.Count - 1 then begin
      FCurrent := FCurrent + 1;
      command := FCommands[FCurrent] as ICommand;
      command.Execute;
    end;
  end;
end;

procedure TUser.Undo(levels: integer);
var
  i: integer;
  command: ICommand;
begin
  WriteLn(#10 + Format('----- Undo %d levels ', [levels]));
  for i := 0 to levels - 1 do begin
    if FCurrent > 0 then begin
      FCurrent := FCurrent - 1;
      command := FCommands[FCurrent] as ICommand;
      command.UnExecute;
    end;
  end;
end;

procedure TUser.Compute(oper: char; operand: integer);
var
  command: ICommand;
begin
  command := TCalculatorCommand.Create(FCalculator, oper, operand);
  command.Execute;
  FCommands.Add(command);
  FCurrent := FCurrent + 1;
end;

end.
unit b_Interpreter;

interface

uses
  SysUtils, StrUtils;

type

  TContext = class
  public
    FInput: string;
    FOutput: integer;
    constructor Create(input: string);
  end;

  TExpression = class
  public
    procedure Interpret(context: TContext);
    function One(): string; virtual; abstract;
    function Four(): string; virtual; abstract;
    function Five(): string; virtual; abstract;
    function Nine(): string; virtual; abstract;
    function Multiplier(): integer; virtual; abstract;
  end;

  TTousandExpression = class(TExpression)
    function One(): string; override;
    function Four(): string; override;
    function Five(): string; override;
    function Nine(): string; override;
    function Multiplier(): integer; override;
  end;

  THundredExpression = class(TExpression)
    function One(): string; override;
    function Four(): string; override;
    function Five(): string; override;
    function Nine(): string; override;
    function Multiplier(): integer; override;
  end;

  TTenExpression = class(TExpression)
    function One(): string; override;
    function Four(): string; override;
    function Five(): string; override;
    function Nine(): string; override;
    function Multiplier(): integer; override;
  end;

  TOneExpression = class(TExpression)
    function One(): string; override;
    function Four(): string; override;
    function Five(): string; override;
    function Nine(): string; override;
    function Multiplier(): integer; override;
  end;

implementation

{ TExpression }

procedure TExpression.Interpret(context: TContext);
var
  start2chars: string;
begin
  if (Length(context.FInput) = 0) then
    exit;

  start2chars := Copy(context.FInput, 0, 2);

  if (LeftStr(context.FInput,1) = Nine) or (start2chars = Nine) then begin
    context.FOutput := context.FOutput + (9 * Multiplier);
    context.FInput := Copy(context.FInput, 3, Length(context.FInput) - 1);
  end
  else if (LeftStr(context.FInput,1) = Four) or (start2chars = Four) then begin
    context.FOutput := context.FOutput + (4 * Multiplier);
    context.FInput := Copy(context.FInput, 3, Length(context.FInput) - 1);
  end
  else if (LeftStr(context.FInput,1) = Five) or (start2chars = Five) then begin
    context.FOutput := context.FOutput + (5 * Multiplier);
    context.FInput := Copy(context.FInput, 2, Length(context.FInput) - 1);
  end;

  while (LeftStr(context.FInput,1) = One) or (start2chars = One) do begin
    context.FOutput := context.FOutput + (1 * Multiplier);
    context.FInput := Copy(context.FInput, 2, Length(context.FInput) - 1);
  end;
end;

{ TTousandExpression }

function TTousandExpression.Five: string;
begin
  Result := '';
end;

function TTousandExpression.Four: string;
begin
  Result := '';
end;

function TTousandExpression.Multiplier: integer;
begin
  Result := 1000;
end;

function TTousandExpression.Nine: string;
begin
  Result := '';
end;

function TTousandExpression.One: string;
begin
  Result := 'M';
end;

{ THundredExpression }

function THundredExpression.Five: string;
begin
  Result := 'D';
end;

function THundredExpression.Four: string;
begin
  Result := 'CD';
end;

function THundredExpression.Multiplier: integer;
begin
  Result := 100;
end;

function THundredExpression.Nine: string;
begin
  Result := 'CM';
end;

function THundredExpression.One: string;
begin
  Result := 'C';
end;

{ TTenExpression }

function TTenExpression.Five: string;
begin
  Result := 'L';
end;

function TTenExpression.Four: string;
begin
  Result := 'XL';
end;

function TTenExpression.Multiplier: integer;
begin
  Result := 10;
end;

function TTenExpression.Nine: string;
begin
  Result := 'XC';
end;

function TTenExpression.One: string;
begin
  Result := 'X';
end;

{ TOneExpression }

function TOneExpression.Five: string;
begin
  Result := 'V';
end;

function TOneExpression.Four: string;
begin
  Result := 'IV';
end;

function TOneExpression.Multiplier: integer;
begin
  Result := 1;
end;

function TOneExpression.Nine: string;
begin
  Result := 'IX';
end;

function TOneExpression.One: string;
begin
  Result := 'I';
end;

{ TContext }

constructor TContext.Create(input: string);
begin
  inherited Create;
  FInput := input;
end;

end.
unit b_Iterator;

interface

uses
  Classes;

type

  TCustomObject = class(TObject)
  public
    FName: string;
    constructor Create(name: string);
    procedure WriteName;
  end;

  TCustomList = class;

  TCustomListEnumerator = class
  private
    FIndex: Integer;
    FCustomList: TCustomList;
  public
    constructor Create(ACustomList: TCustomList);
    function MoveNext: Boolean;
    function GetCurrent: TCustomObject;
    property Current: TCustomObject read GetCurrent;
  end;

  TCustomList = class(TList) 
  public
    procedure Add(ACustomObject: TCustomObject);
    function GetEnumerator: TCustomListEnumerator;
  end;

implementation

{ TCustomObject }

constructor TCustomObject.Create(name: string);
begin
  FName := name;
end;

procedure TCustomObject.WriteName;
begin
  WriteLn('List member ' + FName);
end;

{ TCustomList }

procedure TCustomList.Add(ACustomObject: TCustomObject);
begin
  inherited Add(ACustomObject);
end;

function TCustomList.GetEnumerator: TCustomListEnumerator;
begin
  Result := TCustomListEnumerator.Create(Self);
end;

{ TCustomListEnumerator }

constructor TCustomListEnumerator.Create(ACustomList: TCustomList);
begin
  inherited Create;
  FIndex := -1;
  FCustomList := ACustomList;
end;

function TCustomListEnumerator.GetCurrent: TCustomObject;
begin
  Result := FCustomList.List[FIndex];
end;

function TCustomListEnumerator.MoveNext: Boolean;
begin
  Result := FIndex < FCustomList.Count - 1;
  if Result then
    Inc(FIndex);
end;

end.
unit b_Mediator;

interface

type

  IMediator = interface
  ['{930530D1-A22C-4F46-8B06-066ED153C28C}']
    procedure Send(mess: string; colleague: TObject);
  end;

  TColleague = class
  protected
    FMediator: IMediator;
  public
    constructor Create(mediator: IMediator);
  end;

  TConcreteColleague1 = class(TColleague)
  public
    constructor Create(mediator: IMediator);
    procedure Send(mess: string);
    procedure Notify(mess: string);
  end;

  TConcreteColleague2 = class(TColleague)
  public
    constructor Create(mediator: IMediator);
    procedure Send(mess: string);
    procedure Notify(mess: string);
  end;

  TConcreteMediator = class(TInterfacedObject, IMediator)
  public
    FColleague1: TConcreteColleague1;
    FColleague2: TConcreteColleague2;
    procedure Send(mess: string; colleague: TObject);
  end;

implementation

{ TColleagueBase }

constructor TColleague.Create(mediator: IMediator);
begin
  FMediator := mediator;
end;

{ TConcreteColleague1 }

constructor TConcreteColleague1.Create(mediator: IMediator);
begin
  inherited Create(mediator);
end;

procedure TConcreteColleague1.Notify(mess: string);
begin
  WriteLn('Colleague1 gets message:' + mess);
end;

procedure TConcreteColleague1.Send(mess: string);
begin
  FMediator.Send(mess, Self);
end;

{ TConcreteColleague2 }

constructor TConcreteColleague2.Create(mediator: IMediator);
begin
  inherited Create(mediator);
end;

procedure TConcreteColleague2.Notify(mess: string);
begin
  WriteLn('Colleague2 gets message:' + mess);
end;

procedure TConcreteColleague2.Send(mess: string);
begin
  FMediator.Send(mess, Self);
end;

{ TConcreteMediator }

procedure TConcreteMediator.Send(mess: string; colleague: TObject);
begin
  if colleague = FColleague2 then
    FColleague1.Notify(mess)
  else
    FColleague2.Notify(mess);
end;

end.
unit b_Memento;

interface

uses SysUtils;

type

  TMemento = class
  public
    FName: string;
    FPhone: string;
    FBudget: Double;
    constructor Create(name: string; phone: string; budget: Double);
  end;

  TSalesProspect = class
  private
    FName: string;
    FPhone: string;
    FBudget: Double;
    procedure SetName(value: string);
    procedure SetPhone(value: string);
    procedure SetBudget(value: Double);
  public
    property Name: string read FName write SetName;
    property Phone: string read FPhone write SetPhone;
    property Budget: Double read FBudget write SetBudget;
    function SaveMemento: TMemento;
    procedure RestoreMemento(memento: TMemento);
  end;

  TProspectMemory = class
  public
    FMemento: TMemento;
    destructor Destroy; override;
  end;

implementation

{ TMemento }

constructor TMemento.Create(name, phone: string; budget: Double);
begin
  FName := name;
  FPhone := phone;
  FBudget := budget;
end;

{ TSalesProspect }

procedure TSalesProspect.RestoreMemento(memento: TMemento);
begin
  WriteLn(#10 + ' Restoring State ---' + #10);
  Name := memento.FName;
  Phone := memento.FPhone;
  Budget := memento.FBudget;
end;

function TSalesProspect.SaveMemento: TMemento;
begin
  WriteLn(#10 + ' Saving State ---' + #10);
  Result := TMemento.Create(FName, FPhone, FBudget);
end;

procedure TSalesProspect.SetBudget(value: Double);
begin
  FBudget := value;
  WriteLn('Budget = ' + FloatToStr(FBudget));
end;

procedure TSalesProspect.SetName(value: string);
begin
  FName  := value;
  WriteLn('Name = ' + FName);
end;

procedure TSalesProspect.SetPhone(value: string);
begin
  FPhone := value;
  WriteLn('Phone = ' + FPhone);
end;

{ TProspectMemory }

destructor TProspectMemory.Destroy;
begin
  FMemento.Free;
  inherited;
end;

end.
unit b_Observer;

interface

uses Classes, SysUtils;

type

  IObserver = interface
  ['{A3208B98-3F48-40C6-9986-43B6CB8F4A7E}']
    procedure Update(Subject: TObject);
  end;

  ISubject = interface
  ['{CA063853-73A8-4AFE-8CAA-50600996ADEB}']
    procedure Attach(Observer: IObserver);
    procedure Detach(Observer: IObserver);
    procedure Notify;
  end;

  TStock = class(TInterfacedObject, ISubject)
  private
    FSymbol: string;
    FPrice: Double;
    FInvestors: TInterfaceList;
    function ReadSymbol: string;
    function ReadPrice: Double;
    procedure SetPrice(value: Double);
  public
    constructor Create(symbol: string; price: Double);
    destructor Destroy; override;
    procedure Attach(Observer: IObserver);
    procedure Detach(Observer: IObserver);
    procedure Notify;
    property Symbol: string read ReadSymbol;
    property Price: Double read ReadPrice write SetPrice;
  end;

  TInvestor = class(TINterfacedObject, IObserver)
  private
    FName: string;
  public
    constructor Create(name: string);
    procedure Update(Subject: TObject);
  end;

implementation

{ TStock }

procedure TStock.Attach(Observer: IObserver);
begin
  if FInvestors = nil then
    FInvestors := TInterfaceList.Create;
  if FInvestors.IndexOf(Observer) < 0 then
    FInvestors.Add(Observer);
end;

constructor TStock.Create(symbol: string; price: Double);
begin
  FSymbol := symbol;
  FPrice := price;
end;

destructor TStock.Destroy;
begin
  FInvestors.Free;
  inherited;
end;

procedure TStock.Detach(Observer: IObserver);
begin
  if FInvestors <> nil then
  begin
    FInvestors.Remove(Observer);
    if FInvestors.Count = 0 then
    begin
      FInvestors.Free;
      FInvestors := nil;
    end;
  end;
end;

procedure TStock.Notify;
var
  i: Integer;
begin
  if FInvestors <> nil then
    for i := 0 to Pred(FInvestors.Count) do
      IObserver(FInvestors[i]).Update(Self);
end;

function TStock.ReadPrice: Double;
begin
  Result := FPrice;
end;

function TStock.ReadSymbol: string;
begin
  Result := FSymbol
end;

procedure TStock.SetPrice(value: Double);
begin
  if value <> FPrice then begin
    FPrice := value;
    Notify;    
  end;
end;

{ TInvestor }

constructor TInvestor.Create(name: string);
begin
  FName := name;
end;

procedure TInvestor.Update(Subject: TObject);
begin
  WriteLn(Format('Notified %s of %s change to %g', [FName, TStock(Subject).Symbol, TStock(Subject).Price]));
end;

end.
unit b_State;

interface

type

  TContext = class;

  IState = interface
  ['{2E995182-8FA5-4AF2-9994-6E5B7F273A0B}']
    function MoveUp(c: TContext): integer;
    function MoveDown(c: TContext): integer;
  end;

  TNormalState = class(TInterfacedObject, IState)
  public
    function MoveUp(c: TContext): integer;
    function MoveDown(c: TContext): integer;
  end;

  TFastState = class(TInterfacedObject, IState)
  public
    function MoveUp(c: TContext): integer;
    function MoveDown(c: TContext): integer;
  end;

  TContext = class
  const
    LIMIT = 10;
  public
    FState: IState;
    FCounter: integer;
    constructor Create;
    function Request(i: integer): integer;
  end;

implementation

{ TNormalState }

function TNormalState.MoveDown(c: TContext): integer;
begin
  if c.FCounter < c.LIMIT then begin
    c.FState := TFastState.Create;
    WriteLn('|| ');
  end;
  c.FCounter := c.FCounter - 2;
  Result := c.FCounter;
end;

function TNormalState.MoveUp(c: TContext): integer;
begin
  c.FCounter := c.FCounter + 2;
  Result := c.FCounter;
end;

{ TFastState }

function TFastState.MoveDown(c: TContext): integer;
begin
    if c.FCounter < c.LIMIT then begin
    c.FState := TNormalState.Create;
    WriteLn('|| ');
  end;
  c.FCounter := c.FCounter - 5;
  Result := c.FCounter;
end;

function TFastState.MoveUp(c: TContext): integer;
begin
  c.FCounter := c.FCounter + 5;
  Result := c.FCounter;
end;

{ TContext }

constructor TContext.Create;
begin
  inherited;
  FCounter := LIMIT;
end;

function TContext.Request(i: integer): integer;
begin
  if i = 2 then
    Result := FState.MoveUp(Self)
  else
    Result := FState.MoveDown(Self);
end;

end.
unit b_Strategy;

interface

type

  TContext = class;

  IStrategy = interface
  ['{7F63C143-98D0-4B8C-A02B-894D145BB745}']
    function Move(c: TContext): integer;
  end;

  TStrategy1 = class(TInterfacedObject, IStrategy)
  public
    function Move(c: TContext): integer;
  end;

  TStrategy2 = class(TInterfacedObject, IStrategy)
  public
    function Move(c: TContext): integer;
  end;

  TContext = class
  private
    FStrategy: IStrategy;
  public
    FCounter: integer;
    constructor Create;
    function Algorithm: integer;
    procedure SetStrategy(s: IStrategy);
  end;

implementation

{ TStrategy1 }

function TStrategy1.Move(c: TContext): integer;
begin
  c.FCounter := c.FCounter + 1;
  Result := c.FCounter;
end;

{ TStrategy2 }

function TStrategy2.Move(c: TContext): integer;
begin
  c.FCounter := c.FCounter - 1;
  Result := c.FCounter;
end;

{ TContext }

function TContext.Algorithm: integer;
begin
  Result := FStrategy.Move(Self)
end;

constructor TContext.Create;
begin
  inherited;
  FCounter := 5;
  FStrategy := TStrategy1.Create;
end;

procedure TContext.SetStrategy(s: IStrategy);
begin
  FStrategy := s;
end;

end.
unit b_Template_method;

interface

type

  IPrimitives = interface
  ['{FDD3A7B4-D02B-4F84-8428-C70DED215E42}']
    function Operation1: string;
    function Operation2: string;
  end;

  TClasA = class(TInterfacedObject, IPrimitives)
    function Operation1: string;
    function Operation2: string;
  end;

  TClasB = class(TInterfacedObject, IPrimitives)
    function Operation1: string;
    function Operation2: string;
  end;

  TAlgorithm = class
    procedure TemplateMethod(a: IPrimitives);
  end;

implementation

{ TClasA }

function TClasA.Operation1: string;
begin
  Result := 'ClassA:Op1 ';
end;

function TClasA.Operation2: string;
begin
  Result := 'ClassA:Op2 ';
end;

{ TClasB }

function TClasB.Operation1: string;
begin
  Result := 'ClassB:Op1 ';
end;

function TClasB.Operation2: string;
begin
  Result := 'ClassB:Op2 ';
end;

{ TAlgorithm }

procedure TAlgorithm.TemplateMethod(a: IPrimitives);
var
  s: string;
begin
  s := a.Operation1 + a.Operation2;
  WriteLn(s);
end;

end.
unit b_Visitor;

interface

uses
  Classes, SysUtils;

type

  IVisitor = interface;

  TEmployee = class
  public
    FName: string;
    FIncome: Double;
    FVacationDays: integer;
    constructor Create(nm: string; incm: Double; vacdays: integer);
    procedure Accept(visitor: IVisitor);
  end;

  IVisitor = interface
    procedure Visit(element: TEmployee);
  end;

  TIncomeVisitor = class(TInterfacedObject, IVisitor)
  public
    procedure Visit(element: TEmployee);
  end;

  TVacationVisitor = class(TInterfacedObject, IVisitor)
  public
    procedure Visit(element: TEmployee);
  end;

  TEmployees = class
  private
    FEmployees: TList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Attach(employee: TEmployee);
    procedure Detach(employee: TEmployee);
    procedure Accept(visitor: IVisitor);
  end;

  TClerk = class(TEmployee)
  public
    constructor Create;
  end;

  TDirector = class(TEmployee)
  public
    constructor Create;
  end;

  TPresident = class(TEmployee)
  public
    constructor Create;
  end;

implementation

{ TElement }

procedure TEmployee.Accept(visitor: IVisitor);
begin
  visitor.Visit(Self);
end;

constructor TEmployee.Create(nm: string; incm: Double; vacdays: integer);
begin
  FName := nm;
  FIncome := incm;
  FVacationDays := vacdays;
end;


{ TIncomeVisitor }

procedure TIncomeVisitor.Visit(element: TEmployee);
var
  employee: TEmployee;
begin
  employee := TEmployee(element);
  employee.FIncome := employee.FIncome * 1.10;
  WriteLn(Format('%s new income: %g',[employee.FName, employee.FIncome]));
end;

{ TVacationVisitor }

procedure TVacationVisitor.Visit(element: TEmployee);
var
  employee: TEmployee;
begin
  employee := TEmployee(element);
  employee.FVacationDays := employee.FVacationDays + 3;
  WriteLn(Format('%s new vacation days: %d',[employee.FName, employee.FVacationDays]));
end;

{ TEmployees }

procedure TEmployees.Accept(visitor: IVisitor);
var
  i: integer;
begin
  for i := 0 to FEmployees.Count - 1 do begin
    TEmployee(FEmployees[i]).Accept(visitor);
    WriteLn;
  end;
end;

procedure TEmployees.Attach(employee: TEmployee);
begin
  FEmployees.Add(employee);
end;

constructor TEmployees.Create;
begin
  inherited;
  FEmployees := TList.Create;
end;

destructor TEmployees.Destroy;
var
  i: integer;
begin
  for i := 0 to FEmployees.Count - 1 do  begin
    TEmployee(FEmployees[i]).Free;
  end;
  FEmployees.Free;
  inherited;
end;

procedure TEmployees.Detach(employee: TEmployee);
begin
  FEmployees.Remove(employee);
end;

{ TClerk }

constructor TClerk.Create;
begin
  inherited Create('Hank', 25000.0, 14);
end;

{ TDirector }

constructor TDirector.Create;
begin
  inherited Create('Elly', 35000.0, 16);
end;

{ TPresident }

constructor TPresident.Create;
begin
 inherited Create('Dick', 45000.0, 21);
end;

end.
unit c_Abstract_factory;

interface

type
  // the brands definition
  IBrand = interface
  ['{3D8E5363-08E7-4B8F-ABF1-8953E13C7A9A}']
    function Price: integer;
    function Material: string;
  end;

  TGucci = class(TInterfacedObject, IBrand)
  public
    function Price: integer;
    function Material: string;
  end;

  TPoochy = class(TInterfacedObject, IBrand)
  public
    function Price: integer;
    function Material: string;
  end;
  // the products definition
  IBag = interface
  ['{7EACC8B1-7963-4677-A8D7-9FA62065B880}']
    function Material: string;
  end;

  IShoes = interface
  ['{1560A332-D2D1-43E1-99BB-3D2882C1501A}']
    function Price: integer;
  end;

  TBag = class(TInterfacedObject, IBag)
  private
    FMyBrand: IBrand;
  public
    constructor Create(brand: IBrand);
    function Material: string;
  end;

  TShoes = class(TInterfacedObject, IShoes)
  private
    FMyBrand: IBrand;
  public
    constructor Create(brand: IBrand);
    function Price: integer;
  end;
  // factories definition
  IFactory = interface
  ['{A9948727-DA7C-4843-83CF-92B81DD158F9}']
    function CreateBag: IBag;
    function CreateShoes: IShoes;
  end;

  TGucciFactory = class(TInterfacedObject, IFactory)
  public
    function CreateBag: IBag;
    function CreateShoes: IShoes;
  end;

  TPoochyFactory = class(TInterfacedObject, IFactory)
  public
    function CreateBag: IBag;
    function CreateShoes: IShoes;
  end;

implementation

{ TGucci }

function TGucci.Material: string;
begin
  Result := 'Crocodile skin';
end;

function TGucci.Price: integer;
begin
  Result := 1000;
end;

{ TPoochi }

function TPoochy.Material: string;
begin
  Result := 'Plastic';
end;

function TPoochy.Price: integer;
var
  gucci: TGucci;
begin
  gucci := TGucci.Create;
  try
    Result := Round(gucci.Price / 3);
  finally
    gucci.Free;
  end;
end;

{ TBag }

constructor TBag.Create(brand: IBrand);
begin
  inherited Create;
  FMyBrand := brand;
end;

function TBag.Material: string;
begin
  Result := FMyBrand.Material;
end;

{ TShoes }

constructor TShoes.Create(brand: IBrand);
begin
  inherited Create;
  FMyBrand := brand;
end;

function TShoes.Price: integer;
begin
  Result := FMyBrand.Price;
end;

{ TGucciFactory }

function TGucciFactory.CreateBag: IBag;
begin
  Result := IBag(TBag.Create(TGucci.Create));
end;

function TGucciFactory.CreateShoes: IShoes;
begin
  Result := IShoes(TShoes.Create(TGucci.Create));
end;

{ TPoochyFactory }

function TPoochyFactory.CreateBag: IBag;
begin
  Result := IBag(TBag.Create(TPoochy.Create));
end;

function TPoochyFactory.CreateShoes: IShoes;
begin
  Result := IShoes(TShoes.Create(TPoochy.Create));
end;

end.
unit c_Builder;

interface

uses
  Classes;

type

  TProduct = class
  private
    FParts: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(part: string);
    procedure Display;
  end;

  IBuilder = interface
  ['{9769DCD1-A2F5-4105-B28D-8A34DA6B0C12}']
    procedure BuildPartA;
    procedure BuildPartB;
    function GetResult: TProduct;
  end;

  TBuilder1 = class(TInterfacedObject, IBuilder)
  private
    FProduct: TProduct;
  public
    constructor Create;
    procedure BuildPartA;
    procedure BuildPartB;
    function GetResult: TProduct;
  end;

  TBuilder2 = class(TInterfacedObject, IBuilder)
  private
    FProduct: TProduct;
  public
    constructor Create;
    procedure BuildPartA;
    procedure BuildPartB;
    function GetResult: TProduct;
  end;

  TDirector = class
  public
    procedure Construct(builder: IBuilder);
  end;

implementation

{ TProduct }

procedure TProduct.Add(part: string);
begin
  FParts.Add(part);
end;

constructor TProduct.Create;
begin
  inherited;
  FParts := TStringList.Create;
end;

destructor TProduct.Destroy;
begin
  FParts.Free;
  inherited;
end;

procedure TProduct.Display;
var
  I: integer;
begin
  WriteLn('Product Patrs -----------');
  for I := 0 to FParts.Count - 1 do begin
    WriteLn(FParts[I]);
  end;
end;

{ TBuilder1 }

procedure TBuilder1.BuildPartA;
begin
  FProduct.Add('Part A ');
end;

procedure TBuilder1.BuildPartB;
begin
  FProduct.Add('Part B ');
end;

constructor TBuilder1.Create;
begin
  inherited;
  FProduct := TProduct.Create;
end;

function TBuilder1.GetResult: TProduct;
begin
  Result := FProduct;
end;

{ TBuilder2 }

procedure TBuilder2.BuildPartA;
begin
  FProduct.Add('Part X ');
end;

procedure TBuilder2.BuildPartB;
begin
  FProduct.Add('Part Y ');
end;

constructor TBuilder2.Create;
begin
  inherited;
  FProduct := TProduct.Create;
end;

function TBuilder2.GetResult: TProduct;
begin
  Result := FProduct;
end;

{ TDirector }

procedure TDirector.Construct(builder: IBuilder);
begin
  builder.BuildPartA;
  builder.BuildPartB;
  builder.BuildPartB;
end;

end.
unit c_Factory_Method;

interface

type

  IProduct = interface
  ['{2E0DD8B3-6BA2-4922-93F1-81F521B55AA9}']
    function ShipFrom: string;
  End;

  TProductA = class(TInterfacedObject, IProduct)
    function ShipFrom: string;
  end;

  TProductB = class(TInterfacedObject, IProduct)
    function ShipFrom: string;
  end;

  TDefaultProduct = class(TInterfacedObject, IProduct)
    function ShipFrom: string;
  end;

  TCreator = class
    function FactoryMethod(month: integer): IProduct;
  end;

implementation

{ TProductA }

function TProductA.ShipFrom: string;
begin
  Result := 'from South Africa';
end;

{ TProductB }

function TProductB.ShipFrom: string;
begin
  Result := 'from Spain';
end;

{ TDefaultProduct }

function TDefaultProduct.ShipFrom: string;
begin
  Result := 'not available';
end;

{ TCreator }

function TCreator.FactoryMethod(month: integer): IProduct;
begin
  if (month > 4) and (month <= 11) then begin
    Result := TProductA.Create;
  end else if (month in [1,2,12]) then  begin
    Result := TProductB.Create;
  end else
    Result := TDefaultProduct.Create;
end;

end.
unit c_Prototype;

interface

implementation

end.
unit c_Singleton;

interface

uses SysUtils;

type 
  TSingle = class (Tobject)
  public
    procedure WriteCount;
    class function NewInstance: TObject; override;
    procedure FreeInstance; override;
end;

implementation

var 
  Instance: TObject = nil;
  nCount: Integer = 0;

procedure TSingle.FreeInstance; 
begin
  Dec (nCount);
  if nCount = 0 then
  begin
    inherited FreeInstance;
    Instance := nil;
  end; 
end;

class function TSingle.NewInstance: TObject;
begin
  if not Assigned (Instance) then
    Instance := inherited NewInstance; 
  Result := Instance;
  Inc (nCount); 
end;

procedure TSingle.WriteCount;
begin
  WriteLn('Count = ' + IntToStr(nCount));
end;

end.
unit s_Adapter;

interface

uses SysUtils, Windows;

type
  TAdaptee = class
  public
    function SpecificRequest(a, b: Double): Double;
  end;

  ITarget = interface
  ['{7422D6B2-5601-4DB6-AF78-63524D1ED7A8}']
    function Request(i: Integer): string;
  end;

  TAdapter = class(TAdaptee, ITarget, IInterface)
  private
    FRefCount: Integer;
  public
    function Request(i: Integer): string;
  protected
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  end;

implementation

{ TAdaptee }

function TAdaptee.SpecificRequest(a, b: Double): Double;
begin
  Result := a / b;
end;

{ TAdapter }

function TAdapter.Request(i: Integer): string;
begin
  Result := 'Rough estimate is '
            + IntToStr(Round(SpecificRequest(i, 3)));
end;

function TAdapter.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then
    Result := 0
  else
    Result := E_NOINTERFACE;
end;

function TAdapter._AddRef: Integer;
begin
  Result := InterlockedIncrement(FRefCount);
end;

function TAdapter._Release: Integer;
begin
  Result := InterlockedDecrement(FRefCount);
  if Result = 0 then
    Destroy;
end;

end.
unit s_Bridge;

interface

uses
  SysUtils;

type
  IBridge = interface
  ['{FB1F4FB1-8AD8-48FD-AEA5-1E33B4628879}']
    function OperationImp: String;
  end;

  TAbstraction = class
  private
    FBridge: IBridge;
  public
    constructor Create(implement: IBridge);
    function Operation: string;
  end;

  TImplementationA = class(TinterfacedObject, IBridge)
  public
    function OperationImp: string;
  end;

  TImplementationB = class(TInterfacedObject, IBridge)
  public
    function OperationImp: string;
  end;

implementation

{ TAbstraction }

constructor TAbstraction.Create(implement: IBridge);
begin
  inherited;
  FBridge := implement;
end;

function TAbstraction.Operation: string;
begin
  WriteLn('Abstraction' + ' <<< BRIDGE >>> ' + FBridge.OperationImp);
end;

{ TImplemetationA }

function TImplementationA.OperationImp: string;
begin
  Result := 'Implementation A';
end;

{ TImplementationB }

function TImplementationB.OperationImp: string;
begin
  Result := 'Implementation B';
end;

end.
unit s_Composite;

interface

uses
  SysUtils, Classes;

type
  IComponent = interface
  ['{991C9839-DE75-4687-B194-D820968DBB8E}']
    procedure Add(c: IComponent);
    function Remove(c: IComponent): IComponent;
    function Display(depth: integer): string;
    function Find(c: IComponent): IComponent;
    function GetName: string;
    procedure SetName(Value: string);
    property Name: string read GetName write SetName;
  end;

  TSingle = class(TInterfacedObject, IComponent)
  private
    FName: string;
  public
    constructor Create(n: string);
    destructor Destroy; override;
    procedure Add(c: IComponent);
    function Remove(c: IComponent): IComponent;
    function Display(depth: integer): string;
    function Find(c: IComponent): IComponent;
    function GetName: string;
    procedure SetName(Value: string);
    property Name: string read GetName write SetName;
  end;

  TComposite = class(TInterfacedObject, IComponent)
  private
    FList: TInterfaceList;
    FName: string;
    FHolder: IComponent;
  public
    constructor Create(n: string);
    destructor Destroy; override;
    procedure Add(c: IComponent);
    function Remove(c: IComponent): IComponent;
    function Display(depth: integer): string;
    function Find(c: IComponent): IComponent;
    function GetName: string;
    procedure SetName(Value: string);
    property Name: string read GetName write SetName;
  end;

implementation

{ TComp }

procedure TSingle.Add(c: IComponent);
begin
  WriteLn('Cannot add to an Item');
end;

constructor TSingle.Create(n: string);
begin
  inherited Create;
  SetName(n);
end;

destructor TSingle.Destroy;
begin
  inherited;
end;

function TSingle.Display(depth: integer): string;
begin
  Result := IntToStr(depth) + ' ' + GetName;
end;

function TSingle.Find(c: IComponent): IComponent;
begin
  if c.Name = FName then
    Result := Self
  else
    Result := nil;
end;

function TSingle.GetName: string;
begin
  Result := FName;
end;

function TSingle.Remove(c: IComponent): IComponent;
begin
  WriteLn('Cannot remove directly');
  Result := Self;
end;

procedure TSingle.SetName(Value: string);
begin
  FName := Value;
end;

{ TComposite }

procedure TComposite.Add(c: IComponent);  
begin
  FList.Add(c);
end;

constructor TComposite.Create(n: string);
begin
  inherited Create;
  FHolder := nil;
  FName := n;
  FList := TInterfaceList.Create;
end;

destructor TComposite.Destroy;
var
  i: integer;
begin
  FList.Clear;
  FreeAndNil(FList);
  inherited;
end;

function TComposite.Display(depth: integer): string;
var
  s: String;
  i: Integer;
begin
  s := 'Set ' + FName + ' length: ' + IntToStr(FList.Count) + #10;
  for I := 0 to FList.Count - 1 do begin
    s := s + IComponent(FList.Items[i]).Display(depth + 2) + #10;
  end;
  Result := s;
end;

function TComposite.Find(c: IComponent): IComponent;
var
  f: IComponent;
  i: Integer;
begin
  FHolder := Self;
  if c.Name = FName then begin
    Result := Self;
    Exit;
  end;
  f := nil;
  for i := 0 to FList.Count - 1 do begin
    f := IComponent(FList.Items[i]).Find(c);
    if (f <> nil) then begin
      Result := f;
      exit;
    end;
  end;
  Result := f;
end;

function TComposite.GetName: string;
begin
  Result := FName;
end;

function TComposite.Remove(c: IComponent): IComponent;
var
  i: Integer;
begin
  FHolder := Self;
  if FHolder <> nil then begin
    for i := 0 to FList.Count - 1 do begin
      if ((FList.Items[i] as IComponent).Name = c.Name) then begin
        FList.Remove(c);
      end;
    end;
    Result := FHolder;
  end else begin
    Result := Self;
  end;
end;

procedure TComposite.SetName(Value: string);
begin
  FName := Value;
end;

end.
unit s_Decorator;

interface
  type
    IComponent = interface
      ['{8021ECE2-0D60-4C96-99AA-C5A6C515DF52}']
      function Operation(): String;
    End;

    TComponent = class (TInterfacedObject, IComponent)
    public
      function Operation(): String;
    end;

    TDecoratorA = class (TInterfacedObject, IComponent)
    private
      FComponent: IComponent;
    public
      function Operation(): String;
      constructor Create(c: IComponent);
    end;

    TDecoratorB = class (TInterfacedObject, IComponent)
    private
      FComponent: IComponent;
    public
      addedState: String;
      function Operation(): String;
      function AddedBehaviour(): String;
      constructor Create(c: IComponent);
    end;

    TClient = class
      class procedure Display(s: String; c: IComponent);
    end;


implementation

{ TComponent }

function TComponent.Operation: String;
begin
  Result := 'I am walking ';
end;

{ TDecoratorA }

constructor TDecoratorA.Create(c: IComponent);
begin
  inherited Create;
  Self.FComponent := c;
end;

function TDecoratorA.Operation: String;
var
  s: String;
begin
  s := Self.FComponent.Operation;
  s := s + 'and listening to Classic FM ';
  Result := s;
end;

{ TDecoratorB }

function TDecoratorB.AddedBehaviour: String;
begin
  Result := 'and I bouth a capuccino ';
end;

constructor TDecoratorB.Create(c: IComponent);
begin
  inherited Create;
  Self.FComponent := c;
  Self.addedState := 'past the coffe shop ';
end;

function TDecoratorB.Operation: String;
var
  s: String;
begin
  s := Self.FComponent.Operation;
  s := s + 'to school ';
  Result := s;
end;

{ TClient }

class procedure TClient.Display(s: String; c: IComponent);
begin
  WriteLn(s + c.Operation);
end;

end.
unit s_Facade;

interface

type

  TSystemA = class
  private
    function A1: string;
  end;

  TSystemB = class
  private
    function B1: string;
  end;

  TSystemC = class
  private
    function C1: string;
  end;

  TFacade = class
  public
    class procedure Operation1;
    class procedure Operation2;
  end;

implementation

{ TSystemA }

function TSystemA.A1: string;
begin
  Result := 'System A, Method A1' + #10;
end;

{ TSystemB }

function TSystemB.B1: string;
begin
  Result := 'System B, Method B1' + #10;
end;

{ TSystemC }

function TSystemC.C1: string;
begin
  Result := 'System C, Method C1' + #10;
end;

{ TFacade }

class procedure TFacade.Operation1;
var
  sysA: TSystemA;
  sysB: TSystemB;
  sysC: TSystemC;
begin
  sysA := TSystemA.Create;
  sysB := TSystemB.Create;
  sysC := TSystemC.Create;
  try
    WriteLn('Operation 1' + #10 +
            sysA.A1 +
            sysB.B1 +            
            sysC.C1);

  finally
    sysA.Free;
    sysB.Free;
    sysC.Free;
  end;
end;

class procedure TFacade.Operation2;
var
  sysB: TSystemB;
  sysC: TSystemC;
begin
  sysB := TSystemB.Create;
  sysC := TSystemC.Create;
  try
    WriteLn('Operation 1' + #10 +
            sysB.B1 +
            sysC.C1);

  finally
    sysB.Free;
    sysC.Free;
  end;
end;

end.
unit s_Flyweight;

interface

implementation

type
  IFlyweight = interface
    
  end;

end.
unit s_Proxy;

interface

uses
  SysUtils;

type
  ISubject = interface
  ['{78E26A3C-A657-4327-93CB-F3EB175AF85A}']
    function Request(): string;
  end;

  IProtected = interface
  ['{928BA576-0D8D-47FE-9301-DA3D8F9639AF}']
    function Authenticate(supplied: string): String;
  end;

  TSubject = class
  public
    function Request(): string;
  end;

  TProxy = class (TInterfacedObject, ISubject)
  private
    FSubject: TSubject;
  public
    function Request(): String;
    destructor Destroy(); override;
  end;

  TProtectionProxy = class (TInterfacedObject, ISubject, IProtected)
  private
    FSubject: TSubject;
    const FPassword: String =  'Abracadabra';
  public
    destructor Destroy(); override;
    function Authenticate(supplied: String): String;
    function Request(): String;
  end;

implementation

destructor TProxy.Destroy;
begin
  FreeAndNil(FSubject);
  inherited;
end;

function TProxy.Request: String;
begin
  if FSubject = nil then begin
    WriteLn('Subject Inactive');
    FSubject := TSubject.Create;
  end;
  WriteLn('Subject active');
  Result := 'Proxy: Call to ' + FSubject.Request;
end;

{ TSubject }

function TSubject.Request: string;
begin
  Result := 'Subject Request Choose left door' + #10;
end;

{ TProtectionProxy }

function TProtectionProxy.Authenticate(supplied: String): String;
begin
  if (supplied <> FPassword) then begin
    Result := 'Protection proxy: No Access!';
  end else begin
    FSubject := TSubject.Create;
    Result := 'Protection Proxy: Authenticated';
  end;
end;

destructor TProtectionProxy.Destroy;
begin
  FreeAndNil(FSubject);
  inherited;
end;

function TProtectionProxy.Request: String;
begin
  if FSubject = nil then begin
    Result := 'Protection Proxy: Authenticate first!';
  end else begin
    Result := 'Protection Proxy: Call to ' + FSubject.Request;
  end;
end;

end.
